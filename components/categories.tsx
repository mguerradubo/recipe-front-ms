import React, { useState } from 'react';
import { gql, useQuery } from '@apollo/client';
import Link from 'next/link';
import { Transition } from '@headlessui/react'; 


const CATEGORIES = gql`
    query Categories {
        categories {
            categories{
            id
            name
            }
        }
    }
`;

const Categories: React.FC = () => {
    const { data, loading, error } = useQuery(CATEGORIES);
    const [isDropdownOpen, setIsDropdownOpen] = useState(false);

    if (loading) return <p>Cargando...</p>;
    if (error) return <p>Error: {error.message}</p>;

    return (
        <div className="relative group">
            <button
                onClick={() => setIsDropdownOpen(!isDropdownOpen)}
                className=" group inline-flex items-center px-3 py-2 text-sm font-extrabold sm:text-[17px] text-amber-50 hover:text-red-950 focus:outline-none"
            >
                Categories
                <svg
                    className={`ml-2 h-5 w-5 transform ${
                        isDropdownOpen ? 'rotate-180' : 'rotate-0'
                    } transition-transform`}
                    fill="none"
                    stroke="currentColor"
                    viewBox="0 0 24 24"
                    xmlns="http://www.w3.org/2000/svg"
                >
                    <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        d="M19 9l-7 7-7-7"
                    />
                </svg>
            </button>

            <Transition
                show={isDropdownOpen}
                enter="transition ease-out duration-100 transform"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="transition ease-in duration-75 transform"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
                className="absolute z-10 mt-2 bg-amber-50 border border-gray-300 divide-y divide-amber-100 rounded-lg"
            >
                {data.categories.categories.map((category: any) => (
                    <Link key={category.id} href={`/recipes/category?q=${category.name}`}>
                        <p className="block px-4 py-2 text-sm text-gray-700 hover:bg-red-950 hover:text-amber-50">
                            {category.name}
                        </p>
                    </Link>
                ))}
            </Transition>
        </div>
    );
};

export default Categories;
