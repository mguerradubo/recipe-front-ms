import React, { useEffect, useState } from "react";
import Link from "next/link";
import Image from "next/image";
import SearchRecipe from "./SearchRecipe";
import { getItemFromLocalStorage } from "./LocalStorageUtils";
import Categories from "./categories";

interface Logged {
  isLogged: boolean;
}
interface User {
  email: string;
}

const Navbar: React.FC = () => {
  const [logged, setLogged] = useState(false);
  const user = getItemFromLocalStorage<User>("user");

  useEffect(() => {
    // Realizar la operación de localStorage y actualizar el estado
    const storedIsLogged = getItemFromLocalStorage<Logged>("isLogged");
    if (storedIsLogged) {
      setLogged(storedIsLogged.isLogged);
    }
  }, []);

  const handleLogout = () => {
    localStorage.clear();
    setLogged(false);
  };

  return (
    <header className="background__navbar">
      <nav className="max-w-screen-xl mx-auto flex justify-between items-center py-4 px-6 md:px-0 text-amber-50 font-extrabold sm:text-[17px]">
        <div className="flex items-center">
          <div className="mr-4">
            <Link href="/">
              <Image
                src="/images/recipes_logo3.png"
                alt="logo"
                width={200}
                height={200}
              />
            </Link>
          </div>
          <Categories/>
          <SearchRecipe />
        </div>
        <ul className="hidden md:flex space-x-6">
          {logged ? (
            <div className="flex items-center px-8 ">
              <Link className="hover:text-amber-950" href="/profile/user">{user?.email}</Link>
              <div className="ml-2 flex items-center hover:text-amber-950">
                <button className="p-2.5" onClick={handleLogout}>
                  logout
                </button>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                  stroke="currentColor"
                  strokeWidth="2"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  className="feather feather-log-out"
                >
                  <path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path>
                  <polyline points="16 17 21 12 16 7"></polyline>
                  <line x1="21" y1="12" x2="9" y2="12"></line>
                </svg>
              </div>
            </div>
          ) : (
            <li className=" px-8">
              <Link className="hover:text-amber-950" href="/login">Sign In</Link>
            </li>
          )}
        </ul>
        <button className="md:hidden"></button>
      </nav>
    </header>
  );
};

export default Navbar;
