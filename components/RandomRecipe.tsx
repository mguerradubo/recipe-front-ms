import React from 'react';
import {gql, useQuery} from '@apollo/client';

import RandomRecipesCarousel from './RecipeCarousel';

const GET_RANDOM_RECIPES = gql`
  query GetRandomRecipes{
    random_recipe(input: {limit: 10}){
    recipes{  
      id
      name
      image
      }
    }
  }
`;

interface Recipe {
  _id: string;
  name: string;
  image: string;
}

function RecipesPage() {
  const { loading, error, data } = useQuery(GET_RANDOM_RECIPES);

  if (loading) return <p>Cargando...</p>;
  if (error) return <p>Error: {error.message}</p>;

  const randomRecipes = data.random_recipe.recipes; 

  return (
      <div className='title__carousel'>
        <h1>Random recipes</h1>
        <RandomRecipesCarousel general={randomRecipes} />
      </div>
  );
}

export default RecipesPage;