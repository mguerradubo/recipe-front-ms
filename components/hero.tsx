import React from 'react';

const Hero = () => {
  return (
    <div className="hero">
      <div className="hero2 ">
        <h1 className="hero__title">
        Gastronomy is the art of using food to create happiness.
        </h1>
        <p className="hero__subtitle">(Theodore Zeldin)</p>
      </div>
    </div>
  );
};
export default Hero;