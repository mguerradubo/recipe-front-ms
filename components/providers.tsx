"use client";
import React from "react";
import { ApolloClient, InMemoryCache, ApolloProvider, createHttpLink, split} from "@apollo/client";
import { getMainDefinition } from '@apollo/client/utilities';
import {WebSocketLink} from '@apollo/client/link/ws';

export const Providers = ({ children }: { children: any }) => {
	const httpLink = createHttpLink({
		uri: "http://localhost:4000/graphql",
	})
	
	const wsLink = new WebSocketLink({
		uri: 'ws://localhost:4000/graphql',
		options: {
			reconnect: true
		}
	});
	  const splitLink = split(
		({ query }) => {
		  const definition = getMainDefinition(query);
		  return (
			definition.kind === 'OperationDefinition' &&
			definition.operation === 'subscription'
		  )
		},
		wsLink,
		httpLink,
	  );

	const client = new ApolloClient({
		link: splitLink,
		cache: new InMemoryCache(),
	});
	return <ApolloProvider client={client}>{children}</ApolloProvider>;
};