import React from 'react';

export const setItemToLocalStorage = (key: string, value: any): void => {
    try {
      localStorage.setItem(key, JSON.stringify(value));
    } catch (error) {
      console.error('Error al guardar en localStorage:', error);
    }
  };
  
  export const getItemFromLocalStorage = <T extends unknown>(key: string): T | null => {
    try {
      const item = localStorage.getItem(key);
      return item ? (JSON.parse(item) as T) : null;
    } catch (error) {
      console.error('Error al obtener de localStorage:', error);
      return null;
    }
  };

  