import React from "react";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import dynamic from "next/dynamic";
import Link from "next/link";

const OwlCarousel = dynamic(() => import("react-owl-carousel"), {
  ssr: false,
});

interface Ingredient {
  id: string;
  name: string;
  image: string;
}

const options = {
  loop: true,
  margin: 15,
  items: 4,
  autoplay: true,
};

interface IngredientCarouselProps {
  general: Ingredient[];
}

const IngredientCarousel: React.FC<IngredientCarouselProps> = ({
  general,
}) => {
  return (
    <div className="recipe__carousel">
      <OwlCarousel className="owl-theme" {...options}>
        {general.map((ingredient) => (
          <div key={ingredient.id} className="item">
            <Link href={`/recipes/ingredient?q=${ingredient.id}`}>
            <img
              src={ingredient.image}
              alt={ingredient.name}
            />
            <h2 className=" font-semibold text-amber-50">{ingredient.name}</h2>
            </Link>
          </div>
        ))}
      </OwlCarousel>
    </div>
  );
};

export default IngredientCarousel;