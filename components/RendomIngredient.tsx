import React from 'react';
import {gql, useQuery} from '@apollo/client';

import RandomCarousel from './IngredientCarousel';

const GET_RANDOM_INGREDIENT = gql`
  query GetRandomRecipes{
    random_ingredient(input: {limit: 15}){
    ingredients{
      id
      name
      image
      }
    }
  }
`;

interface Ingredient {
  id: string;
  name: string;
  image: string;
}

function IngredientPage() {
  const { loading, error, data } = useQuery(GET_RANDOM_INGREDIENT);

  if (loading) return <p>Cargando...</p>;
  if (error) return <p>Error: {error.message}</p>;

  const randomIngredient = data.random_ingredient.ingredients; 

  return (
      <div className='title__carousel'>
        <h1>Random ingredients</h1>
        <RandomCarousel general={randomIngredient} />
      </div>
  );
}

export default IngredientPage;