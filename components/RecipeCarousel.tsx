import React from "react";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import dynamic from "next/dynamic";
import Link from "next/link";

const OwlCarousel = dynamic(() => import("react-owl-carousel"), {
  ssr: false,
});

interface Recipe {
  id: string;
  name: string;
  image: string;
}

const options = {
  loop: true,
  margin: 15,
  items: 4,
  autoplay: true,
};

interface RecipeCarouselProps {
  general: Recipe[];
}

const RecipeCarousel: React.FC<RecipeCarouselProps> = ({
  general,
}) => {
  return (
    <div className="recipe__carousel">
      <OwlCarousel className="owl-theme" {...options}>
        {general.map((recipe) => (
          <div key={recipe.id} className="item">
            <Link href={`/recipes/meal?q=${recipe.id}`}>
            <img
              src={recipe.image}
              alt={recipe.name}
            />
            <h2 className=" font-semibold text-amber-50">{recipe.name}</h2>
            </Link>
          </div>
          
        ))}
      </OwlCarousel>
    </div>
  );
};

export default RecipeCarousel;
