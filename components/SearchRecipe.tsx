import React, { useState } from "react";

const SearchRecipe: React.FC = () => {
  const [query, setQuery] = useState('');

  return (
    <form action="/recipes/search" method="get" className="search-form">
      <div className="flex">
        <input
          type="text"
          name="q"
          value={query}
          onChange={(e) => setQuery(e.target.value)}
          placeholder="Search recipes"
          className="text-black font-bold md:text-left mb-1 md:mb-0 pr-4 focus:outline-none"
        />
        <button
          type="submit"
          className="bg-red-900 hover:bg-amber-950 text-white font-bold py-2.5 px-3 rounded p-2.5 ml-2 text-sm"
        >
          <svg
            className="w-4 h-4"
            aria-hidden="true"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 20 20"
          >
            <path
              stroke="currentColor"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
            />
          </svg>
          <span className="sr-only">Search</span>
        </button>
      </div>
    </form>
  );
};

export default SearchRecipe;
