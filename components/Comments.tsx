import React, { useState, useEffect } from "react";
import { useMutation, gql, useSubscription } from "@apollo/client";
import { getItemFromLocalStorage } from "./LocalStorageUtils";
import { useRouter } from "next/router";

interface CommentFormProps {
  recipeID: string;
  comments: any[];
}

interface Logged {
  isLogged: boolean;
}
interface User {
  email: string;
}

const ADD_COMMENT_MUTATION = gql`
  mutation AddComment($text: String!, $recipeID: String!, $email: String!) {
    addComment(input: { text: $text, recipeId: $recipeID, email: $email }) {
      id
      text
      createdAt
      recipeId
    }
  }
`;
const COMMENT_ADDED_SUBSCRIPTION = gql`
  subscription CommentAdded($recipeId: String!) {
    commentAdded(recipeId: $recipeId) {
      id
      text
      createdAt
      email
    }
  }
`;

const REMOVE_COMMENT = gql`
  mutation RemoveComment($id: String!, $recipeId: String!) {
    removeComment(input: {id: $id, recipeId: $recipeId}){
      success
    }
  }
`;
const CommentForm: React.FC<CommentFormProps> = ({ recipeID, comments }) => {
  const router = useRouter();
  const [text, setText] = useState("");

  const [logged, setLogged] = useState(false);
  const [addComment] = useMutation(ADD_COMMENT_MUTATION);
  const [removeComment] = useMutation(REMOVE_COMMENT);
  const { data, error} = useSubscription(COMMENT_ADDED_SUBSCRIPTION, {
    variables: { recipeId: recipeID },
    onSubscriptionData: ({ subscriptionData }) => {
      if (subscriptionData.data) {
        const newComment = subscriptionData.data.commentAdded;
        setCommentList((prevComments) => {
          if (Array.isArray(prevComments)) {
            return [...prevComments, newComment];
          } else {
            return [newComment]; // Si prevComments no es un array, crea uno nuevo
          }
        });
      }
    },
  });
  if (error) return <p>Error: {error.message}</p>;
  const storedIsLogged = getItemFromLocalStorage<Logged>("isLogged");
  const [commentList, setCommentList] = useState(comments);

  const user = getItemFromLocalStorage<User>("user");

  useEffect(() => {
    if (storedIsLogged) {
      setLogged(storedIsLogged.isLogged);
    }
  }, []);

  const handleCommentSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    if (!logged) {
      router.push("/login");
    }

    try {
      const { data } = await addComment({
        variables: { text, recipeID, email: user?.email },
      });

      setText("");
      if (data.addComment) {
        // Si se agrega el comentario exitosamente, no es necesario esperar la notificación de la suscripción
        const newComment = data.addComment; // Utiliza la respuesta de la mutación
      }
    } catch (error) {
      console.error("Error al agregar el comentario", error);
    }
  };

  const handleCommentRemove = async (commentId: string) => {
    try {
      await removeComment({ variables: { id: commentId , recipeId: recipeID} });

      // Actualiza la lista de comentarios después de eliminar uno
      setCommentList((prevComments) =>
        prevComments.filter((comment) => comment.id !== commentId)
      );
    } catch (error) {
      console.error("Error al eliminar el comentario", error);
    }
  };
  return (
    <div className="p-2">
      <div className="text-amber-50 my-4">
        <h2 className="font-semibold">Comments:</h2>
        {commentList && commentList.length > 0 ? (
          commentList.map((comment: any) => (
            <div key={comment.id}>
              <div className="w-full p-2 bg-origin-border rounded-lg shadow-lg my-4 bg-red-900">
                <p className="text-amber-50">
                  <strong>
                    {comment.email === user?.email ? "You" : comment.email}
                  </strong>
                </p>
                <p className="font-light">{comment.text}</p>
                <p className="text-gray-400">
                  {
                   comment.createdAt.split("T")[0] 
                  }
                  
                </p>
                {user && user.email === comment.email && (
                  <button
                    onClick={() => handleCommentRemove(comment.id)}
                    className="bg-red-900 hover:bg-red-800 text-white font-semibold py-1 px-2 rounded-md"
                  >
                    Delete
                  </button>
                )}
              </div>
            </div>
          ))
        ) : (
          <p>No comments available.</p>
        )}
      </div>
      <form onSubmit={handleCommentSubmit}>
        <textarea
          className="w-full p-2 border rounded-md text-black"
          rows={4}
          placeholder="Write your comment here..."
          value={text}
          onChange={(e) => setText(e.target.value)}
        />
        <button
          type="submit"
          className="mt-2 bg-amber-950 hover:bg-red-900 text-white font-semibold py-2 px-4 rounded-md"
        >
          Add Comment
        </button>
      </form>
    </div>
  );
};

export default CommentForm;
