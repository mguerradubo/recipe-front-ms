describe('template spec', () => {
  it('login form', () => {
    cy.viewport(1920, 1080)
    cy.visit('/')
    cy.visit('/login')
    cy.get('.mb-4 > .w-full').type('marcelo@gmail.com')
    cy.get('.mb-2 > .w-full').type(Cypress.env('password'), {
      log: false,
    })
    cy.get('form').submit()
  })
  it('profile page', () => {
    cy.viewport(1920, 1080)
    cy.visit('/')
    cy.visit('/login')
    cy.get('.mb-4 > .w-full').type('marcelo@gmail.com')
    cy.get('.mb-2 > .w-full').type(Cypress.env('password'), {
      log: false,
    })
    cy.get('form').submit()
    cy.visit('/profile/user')

  })

})