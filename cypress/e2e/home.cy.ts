describe('template spec', () => {
  it('passes', () => {
    cy.viewport(1920, 1080)
    cy.visit('/')
  });
  
  it("click navbar search", () => {
    cy.viewport(1920, 1080)
    cy.visit('/')
    cy.get('.text-black').type('empanada')
    cy.get('.bg-red-900').click()
  });

  it("click meal", () =>{
    cy.viewport(1920, 1080)
    cy.visit('/')
    cy.get(':nth-child(4) > .recipe__carousel > .owl-carousel > .owl-stage-outer > .owl-stage > :nth-child(14) > .item > a > img').click()
  })

  it("click ingredient", () =>{
    cy.viewport(1920, 1080)
    cy.visit('/')
    cy.get(':nth-child(5) > .recipe__carousel > .owl-carousel > .owl-stage-outer > .owl-stage > :nth-child(11) > .item > a > img')
  })
})