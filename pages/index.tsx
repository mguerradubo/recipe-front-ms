import 'tailwindcss/tailwind.css';
import Hero from "@/components/hero";
import Navbar from "@/components/navbar";

import RandomRecipes from "../components/RandomRecipe";
import RandomIngredients from "@/components/RendomIngredient";

export default function Page() {
    return <div>
        <div className="global__background"></div>
        <Navbar/>
        <Hero/>
        <RandomRecipes/>
        <RandomIngredients/>
        </div>
  }