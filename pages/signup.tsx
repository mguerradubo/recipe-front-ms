import { useState } from "react";
import Link from "next/link";
import Image from "next/image";
import { gql, useMutation } from "@apollo/client";
import { useRouter } from "next/router";

const LOGIN_MUTATION = gql`
mutation SignUp($email: String!, $password: String!, $name: String!, $lastname: String!){
  signUp(input: {
    email: $email
    password: $password
    name: $name
    lastname: $lastname
  })
}
`;
export default function SignUp() {
  const router = useRouter();
  const [name, setName] = useState('');
  const [lastname, setLastname] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [SignUp, { loading, error}] = useMutation(LOGIN_MUTATION);

  if (loading) return <p>Cargando...</p>;
  if (error) return <p>Error: {error.message}</p>;
  
  const handleSignUp = async () => {
    try {
      const { data } = await SignUp({
        variables: { email, password, name, lastname },
      });
      const token = data.login;  
      router.back();
    } catch (error) {
      console.error('Error al iniciar sesión:', error);
    }
  };
  function handleFormSubmit(event: { preventDefault: () => void; }) {
    event.preventDefault(); // Evita la recarga de la página
    handleSignUp(); // Llama a tu función de inicio de sesión
  }

  return (
    <div className="relative flex flex-col items-center justify-center min-h-screen overflow-hidden">
      <div className="global__background"></div>
      <div className="w-full p-6 bg-white rounded-md shadow-md lg:max-w-xl">
        <div className="flex items-center justify-center">
        <Link href="/">
          <Image
            src="/images/recipes_logo3.png"
            alt="logo"
            width={220}
            height={220}
          />
          </Link>
        </div>
        <form onSubmit={handleFormSubmit} className="mt-6">
        <div className="mb-4">
            <label
              htmlFor="name"
              className="block text-sm font-semibold text-gray-800"
            >
              Name
            </label>
            <input
              type="name"
              value={name}
              onChange={(e) => setName(e.target.value)}
              className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border rounded-md focus:border-gray-400 focus:ring-gray-300 focus:outline-none focus:ring focus:ring-opacity-40"
            />
          </div>
          <div className="mb-4">
            <label
              htmlFor="name"
              className="block text-sm font-semibold text-gray-800"
            >
              Lastname
            </label>
            <input
              type="lastname"
              value={lastname}
              onChange={(e) => setLastname(e.target.value)}
              className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border rounded-md focus:border-gray-400 focus:ring-gray-300 focus:outline-none focus:ring focus:ring-opacity-40"
            />
          </div>
          <div className="mb-4">
            <label
              htmlFor="email"
              className="block text-sm font-semibold text-gray-800"
            >
              Email
            </label>
            <input
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border rounded-md focus:border-gray-400 focus:ring-gray-300 focus:outline-none focus:ring focus:ring-opacity-40"
            />
          </div>
          <div className="mb-2">
            <label
              htmlFor="password"
              className="block text-sm font-semibold text-gray-800"
            >
              Password
            </label>
            <input
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border rounded-md focus:border-gray-400 focus:ring-gray-300 focus:outline-none focus:ring focus:ring-opacity-40"
            />
          </div>
          <div className="mt-10">
            <button type="submit" className="w-full px-4 py-2 tracking-wide text-white transition-colors duration-200 transform bg-gray-700 rounded-md hover:bg-gray-600 focus:outline-none focus:bg-gray-600">
              Sign Up
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
