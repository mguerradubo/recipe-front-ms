import { useState } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import Image from "next/image";
import {gql, useMutation} from '@apollo/client';
import { setItemToLocalStorage} from "@/components/LocalStorageUtils";

interface Logged {
  isLogged: boolean;
}

const LOGIN_MUTATION = gql`
mutation Login($email: String!, $password: String!){
  login(input: {
    email: $email
    password: $password
  })
}
`;
export default function Login() {
  const router = useRouter();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [login, { loading, error}] = useMutation(LOGIN_MUTATION);

  if (loading) return <p>Cargando...</p>;
  if (error) return <p>Error: {error.message}</p>;
  
  const handleLogin = async () => {
    try {
      const { data } = await login({
        variables: { email, password },
      });
      const token = data.login;

      setItemToLocalStorage('token', { token: token });
      setItemToLocalStorage('user', { email: email });
      setItemToLocalStorage('isLogged', { isLogged: true });
  
      router.back();
    } catch (error) {
      console.error('Error al iniciar sesión:', error);
    }
  };
  function handleFormSubmit(event: { preventDefault: () => void; }) {
    event.preventDefault(); // Evita la recarga de la página
    handleLogin(); // Llama a tu función de inicio de sesión
  }
  return (
    <div className="relative flex flex-col items-center justify-center min-h-screen overflow-hidden">
      <div className="global__background"></div>
      <div className="w-full p-6 bg-white rounded-md shadow-md lg:max-w-xl">
        <div className="flex items-center justify-center">
        <Link href="/">
          <Image
            src="/images/recipes_logo3.png"
            alt="logo"
            width={220}
            height={220}
          />
          </Link>
        </div>
        <form onSubmit={handleFormSubmit} className="mt-6">
          <div className="mb-4">
            <label
              htmlFor="email"
              className="block text-sm font-semibold text-gray-800"
            >
              Email
            </label>
            <input
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border rounded-md focus:border-gray-400 focus:ring-gray-300 focus:outline-none focus:ring focus:ring-opacity-40"
            />
          </div>
          <div className="mb-2">
            <label
              htmlFor="password"
              className="block text-sm font-semibold text-gray-800"
            >
              Password
            </label>
            <input
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border rounded-md focus:border-gray-400 focus:ring-gray-300 focus:outline-none focus:ring focus:ring-opacity-40"
            />
          </div>
          <Link
            href="/forget"
            className="text-xs text-blue-600 hover:underline"
          >
            Forget Password?
          </Link>
          <div className="mt-2">
            <button type="submit" className="w-full px-4 py-2 tracking-wide text-white transition-colors duration-200 transform bg-gray-700 rounded-md hover:bg-gray-600 focus:outline-none focus:bg-gray-600">
              Login
            </button>
          </div>
        </form>

        <p className="mt-4 text-sm text-center text-gray-700">
          Don't have an account?{" "}
          <Link
            href="/signup"
            className="font-medium text-blue-600 hover:underline"
          >
            Sign up
          </Link>
        </p>
      </div>
    </div>
  );
}
