import React from "react";
import { useRouter } from "next/router";
import { gql, useQuery } from "@apollo/client";
import Navbar from "@/components/navbar";
import CommentForm from "@/components/Comments";
import Link from "next/link";

const MEAL_RECIPES = gql`
  query Recipe($query: String!) {
    recipe(input: {id: $query}) {
      id
      name
      category
      instruction
      ingredients {
        name
        image
      }
      measure
      video
      image
      comments {
        text
        createdAt
        email
      }
    }
  }
`;

function convertirEnlaceAEmbed(enlace: string): string | null {
  // Extraer el identificador del video del enlace
  const match = enlace.match(/v=([a-zA-Z0-9_-]+)/);

  if (match && match[1]) {
    const identificadorVideo = match[1];
    return `https://www.youtube.com/embed/${identificadorVideo}`;
  } else {
    // Si el enlace no es válido, puedes manejar el caso de error aquí
    return null;
  }
}

const MealPage: React.FC = () => {
  const router = useRouter();
  const query = router.query.q as string;
  const { loading, error, data } = useQuery(MEAL_RECIPES, {
    variables: { query },
  });
 
  if (loading) return <p>Cargando...</p>;
  if (error) return <p>Error: {error.message}</p>;

  const recipe = data.recipe;
  const enlaceEmbed: string | null = convertirEnlaceAEmbed(recipe.video);

  return (
    <div>
      <Navbar />
      <div className="global__background"></div>
      <div className="px-4 py-8 md:px-12 md:py-16 max-w-screen-xl mx-auto">
        <h1 className="text-3xl font-semibold text-center text-amber-50 mb-6">
          {recipe.name}
        </h1>
        <div className="flex flex-wrap justify-center gap-8">
          <div className="w-full md:w-1/2 flex-initial">
            <img
              src={recipe.image}
              alt={recipe.name}
              className="w-full h-auto"
            />
          </div>
          <div className="w-full md:w-1/2 flex-1">
            <div className="text-amber-50 mb-4">
              <h2 className="font-semibold">Category:</h2>
              <Link href={`/recipes/category?q=${recipe.category}`}><p className=" hover:text-red-950">{recipe.category}</p></Link>
            </div>
            <div className="text-amber-50 mb-4">
              <h2 className="font-semibold">Ingredients:</h2>
              <ul>
                {recipe.ingredients.map((ingredient: any, index: number) => (
                  <li key={ingredient.name}>
                    {ingredient.name} - {recipe.measure[index]}
                  </li>
                ))}
              </ul>
            </div>
            <div className="text-amber-50 mb-4">
              <h2 className="font-semibold">Instructions:</h2>
              <p className="font-light">{recipe.instruction}</p>
            </div>
          </div>
        </div>
        <div className="text-amber-50 mt-8">
          <div className="video-container">
            <iframe
              width="100%"
              height="500"
              src={enlaceEmbed || ""}
              title={recipe.name}
            ></iframe>
          </div>
        </div>
        <div className="text-amber-50 mt-10 border p-4 rounded-lg shadow-lg my-4">        
          <div>
            <CommentForm recipeID={recipe.id} comments={recipe.comments} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default MealPage;
