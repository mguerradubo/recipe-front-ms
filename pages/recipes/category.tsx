// pages/results.tsx
import React from 'react';
import { useRouter } from "next/router";
import { gql, useLazyQuery } from "@apollo/client";
import Navbar from '@/components/navbar';
import Link from "next/link";

const SEARCH_RECIPES = gql`
  query CategoryRecipe($category: String!){
    category_recipes(input: {category: $category}){
      recipes{
        id
        name
        image
      }
    }
  }
`;

const CategoryPage: React.FC = () => {
  const router = useRouter();
  const category = router.query.q as string;
  const [categoryRecipe, { loading, data, error }] = useLazyQuery(SEARCH_RECIPES, {
    variables: {category },
  });
  React.useEffect(() => {
    if (category) {
      categoryRecipe();
    }
  }, [category]);

  if (loading) {
    return <p>Cargando...</p>;
  }
  if (error) {
    return <p>Error: {error.message}</p>;
  }

  return (
    <div>
      <Navbar />
      <div className="global__background"></div>
      <h1 className="text-3xl font-semibold text-center text-amber-50 mb-6">
          {category}
        </h1>
      <div className='px-12 py-4'>
        {data && (
          <div className="flex justify-between gap-2 flex-wrap ">
            {data.category_recipes.recipes.map((recipe: any) => (
              <Link href={`/recipes/meal?q=${recipe.id}`} key={recipe.id}>
                <div className="grid">
                  <img src={recipe.image} alt={recipe.name} className="h-auto w-80 max-w-[1440px] mx-auto py-4" />
                  <h2 className="font-semibold text-amber-50">{recipe.name}</h2>
                </div>
              </Link>
            ))}
          </div>
        )}
      </div>
    </div>
  );
};

export default CategoryPage;
