// pages/results.tsx
import React from 'react';
import { useRouter } from "next/router";
import { gql, useLazyQuery } from "@apollo/client";
import Navbar from '@/components/navbar';
import Link from "next/link";

const SEARCH_RECIPES = gql`
  query SearchRecipe($query: String!){
    searchRecipe(input: {query: $query}){
      recipes{
        id
        name
        image
      }
    }
  }
`;

const ResultsPage: React.FC = () => {
  const router = useRouter();
  const query = router.query.q as string;
  const [searchRecipe, { loading, data, error }] = useLazyQuery(SEARCH_RECIPES, {
    variables: { query },
  });

  React.useEffect(() => {
    if (query) {
      searchRecipe();
    }
  }, [query]);

  if (loading) {
    return <p>Cargando...</p>;
  }
  if (error) {
    return <p>Error: {error.message}</p>;
  }

  return (
    <div>
      <Navbar />
      <div className="global__background"></div>
      <div className='px-12 py-4'>
        {data && (
          <div className="flex justify-between gap-2 flex-wrap ">
            {data.searchRecipe.recipes.map((recipe: any) => (
              <Link href={`/recipes/meal?q=${recipe.id}`} key={recipe.id}>
                <div className="grid">
                  <img src={recipe.image} alt={recipe.name} className="h-auto w-80 max-w-[1440px] mx-auto py-4" />
                  <h2 className="font-semibold text-amber-50">{recipe.name}</h2>
                </div>
              </Link>
            ))}
          </div>
        )}
      </div>
    </div>
  );
};

export default ResultsPage;
