import React from "react";
import { useRouter } from "next/router";
import { gql, useQuery } from "@apollo/client";
import Navbar from "@/components/navbar";

const INGREDIENT= gql`
query Ingredient($query: String!){
    ingredient(input: {id: $query}){
      name
      description
      image
    }
  }
`;
const MealPage: React.FC = () => {
  const router = useRouter();
  const query = router.query.q as string;
  const { loading, error, data } = useQuery(INGREDIENT, {
    variables: { query },
  });

  if (loading) return <p>Cargando...</p>;
  if (error) return <p>Error: {error.message}</p>;

  const ingredient = data.ingredient;

  return (
    <div>
      <Navbar />
      <div className="global__background"></div>
      <div className="px-4 py-8 md:px-12 md:py-16 max-w-screen-xl mx-auto">
        <h1 className="text-3xl font-semibold text-center text-amber-50 mb-6">
          {ingredient.name}
        </h1>
        <div className="flex flex-wrap items-center justify-center gap-4">
          <div className="w-full md:w-1/2">
            <img
              src={ingredient.image}
              alt={ingredient.name}
              className="w-full h-auto"
            />
          </div>
          <div className="w-full md:w-1/2">
            <div className="text-amber-50 mb-4">
              <h2 className="font-semibold">Description:</h2>
              <p className="font-light">{ingredient.description || "No description"}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MealPage;
