import Navbar from "@/components/navbar";
import { getItemFromLocalStorage } from "@/components/LocalStorageUtils";
import { gql, useQuery } from "@apollo/client";

const USER = gql`
  query User($email: String!){
    user(email: $email){
      name
      lastname
      email
    }
  }`;

interface Token {
  token: string;
}

interface User {
  email: string;
}

const Profile = () => {
  const user = getItemFromLocalStorage<User>("user");
  const token = getItemFromLocalStorage<Token>("token");

  const { data, loading, error } = useQuery(USER, {
    variables: { email: user?.email },
    context: { headers: { authorization: `Bearer ${token?.token}`} },
  });

  if (loading) return <p>Cargando...</p>;
  if (error) return <p>Error: {error.message}</p>;

  return (
    <div>
      <Navbar />
      <div className="global__background"></div>
      <div className="p-4 md:p-8">
        <div className=" bg-amber-50 rounded-lg shadow-md p-4 md:p-8">
          <h1 className="text-3xl font-semibold">Perfil de Usuario</h1>
          <div className="mt-4">
            <p className="text-xl">Nombre: {data.user.name}</p>
            <p className="text-xl">Apellido: {data.user.lastname}</p>
            <p className="text-xl">Email: {data.user.email}</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Profile;
